FROM python:3.12-alpine

WORKDIR /src

COPY ./src /src

COPY requirements.txt .

RUN pip install -r requirements.txt

ENTRYPOINT ["fastapi", "dev", "/src/app/main.py"]
