from typing import Annotated

from fastapi import Depends
from sqlalchemy.orm import Session
from app.schemas import schemas
from app.utils.database import get_db
from app.models import models

def list_experiments(user: schemas.User, db: Annotated[Session, Depends(get_db)]):
    return db.query(models.User, models.Experiment).filter(models.User.id == user.id).filter(models.Experiment.user_id == user.id).all()

def get_experiment_by_id(experiment_id:int, db: Session):
    return db.query(models.Experiment).filter(models.Experiment.id == experiment_id).first()

def create_experiment(experiment: schemas.ExperimentCreate, db: Session):
    experiment_model = models.Experiment(description=experiment.description, model_id=experiment.model_id, user_id=experiment.user_id)
    db.add(experiment_model)
    db.commit()
    db.refresh(models.Experiment)
    return experiment_model
