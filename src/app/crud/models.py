from typing import Annotated
from fastapi import Depends, HTTPException, status
from sqlalchemy.orm import Session, query

from app.utils.database import get_db
from app.models.models import Model

def create_model(model_name: str, db: Annotated[Session, Depends(get_db)]):
    model_exists = db.query(Model).filter(Model.name == model_name).first() != None
    if model_exists:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Model with that name "
        )
    model = Model(name=model_name)
    db.add(model)
    db.commit()
    return model

def get_model_by_id(model_id: int, db: Session):
    return db.query(Model).filter(Model.id == model_id).first()

def get_models(db: Session):
    return db.query(Model).all()
