from re import L
from typing import Annotated
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from datetime import datetime, timedelta
import jwt
from app.models.models import User as UserModel
from app.exceptions import invalid_credentials, unauthenticated_exception
from app.schemas import schemas
from app.utils.auth import hash_password, verify_password, Token, oauth2_scheme
from app.utils.database import get_db

SECRET_KEY = "594be298b860ce2a87b026a6fdae1b40676b8f56d0a1cdb1249efb34ea1f5d3a"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30


def create_user(user: schemas.UserCreate, db: Annotated[Session, Depends(get_db)]):
    if get_user_by_username(user.username, db):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="A user with that username already exists.")
    hashed_password = hash_password(user.password)
    db_user = UserModel(username=user.username, hashed_password=hashed_password, admin=user.admin)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

def get_user_by_username(username: str, db: Session) -> schemas.User:
    return db.query(UserModel).filter(UserModel.username == username).first()

def get_user_by_id(user_id: int, db: Session) -> schemas.User:
    return db.query(UserModel).filter(UserModel.id == user_id).first()

def get_current_user(token: Annotated[str, Depends(oauth2_scheme)], db: Session = Depends(get_db)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = username
    except:
        raise credentials_exception
    user =  get_user_by_username(username=token_data, db=db)
    if user is None:
        raise credentials_exception
    return user

def get_current_admin_user(user: Annotated[schemas.User, Depends(get_current_user)]):
    if not user.admin:
        raise unauthenticated_exception
    return user

def create_access_token(data: dict, expires_delta: timedelta | None = None) -> str:
    data = data.copy()
    expires_at = datetime.now()
    if expires_delta:
        expires_at += expires_delta
    else:
        expires_at += timedelta(ACCESS_TOKEN_EXPIRE_MINUTES)
    data.update({"exp": expires_at})
    encoded_jwt: str = jwt.encode(payload=data, key=SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt

def login(form_data: Annotated[OAuth2PasswordRequestForm, Depends()], db: Annotated[Session, Depends(get_db)]):
    user =  get_user_by_username(form_data.username, db)
    if not user or not verify_password(form_data.password, user.hashed_password):
        raise invalid_credentials
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token =  create_access_token(data={"sub": form_data.username, "scopes":form_data.scopes},expires_delta=access_token_expires)    
    return Token(access_token=access_token,token_type="bearer")

def get_all_users(db: Session):
    return db.query(UserModel).all()
