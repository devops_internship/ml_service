from fastapi import HTTPException, status

unauthenticated_exception = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Could not validate credentials."
)

invalid_credentials = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Invalid username or password.",
    headers={"WWW-Authenticate": "Bearer"}
)

user_isnt_admin = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="No priviliges for operation"
)

