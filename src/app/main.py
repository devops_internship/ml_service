from typing import Annotated
from fastapi import FastAPI
from fastapi.security import OAuth2PasswordRequestForm
from .routers import users, experiments, models
from .utils.database import Base, engine

Base.metadata.create_all(bind=engine)

app = FastAPI()
app.include_router(models.router)
app.include_router(experiments.router)
app.include_router(users.router)


