from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship 
from app.utils.database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    username = Column(String, index=True, unique=True)
    hashed_password = Column(String)
    admin = Column(Boolean)

    experiments = relationship("Experiment", back_populates="user")

class Model(Base):
    __tablename__ = "models"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, index=True)

    experiments = relationship("Experiment", back_populates="model")


class Experiment(Base):
    __tablename__ = "experiments"

    id = Column(Integer, primary_key=True)
    description = Column(String, index=True)
    model_id = Column(Integer, ForeignKey("models.id"))
    user_id = Column(Integer, ForeignKey("users.id")) 

    user = relationship("User", back_populates="experiments")
    model = relationship("Model", back_populates="experiments")
    metrics = relationship("Metric", back_populates="experiments")

class Metric(Base):
    __tablename__ = "metrics"
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, index=True)
    metric_type = Column(String)
    value = Column(String)
    experiment_id = Column(Integer, ForeignKey("experiments.id"))

    experiments = relationship("Experiment", back_populates="metrics")
