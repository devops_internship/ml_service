from typing import Annotated
from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app.crud.users import get_current_user
from app.schemas import schemas
from app.crud import experiments as crud_experiments
from app.utils.database import get_db
from app.schemas import schemas

router = APIRouter(prefix="/experiments", tags=["experiments"])

@router.get("/")
def get_all_users_experiments(user: Annotated[schemas.User, Depends(get_current_user)], db: Annotated[Session, Depends(get_db)]):
    return crud_experiments.list_experiments(user, db)

@router.post("/", response_model=schemas.Experiment)
def create_experiment(experiment: Annotated[schemas.ExperimentCreate, Body()], db: Annotated[Session, Depends(get_db)]):
    return crud_experiments.create_experiment(experiment, db)

@router.get("/{experiment_id}")
def get_experiment_info(experiment_id: int, user: Annotated[schemas.User, Depends(get_current_user)], db: Annotated[Session, Depends(get_db)]):
    experiment: schemas.ExperimentCreate = crud_experiments.get_experiment_by_id(experiment_id, db)
    if experiment.user_id != user.id:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="No permission to view this experiment.")
    return experiment
