from typing import Annotated, List
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from app.schemas import schemas 
import app.crud.models as crud_models
from app.models import models
from app.utils.database import get_db
from app.schemas.schemas import ModelCreate
from app.crud.users import get_current_admin_user

router = APIRouter(prefix="/models", tags=["models"]) 

@router.get("/{model_id}")
def get_model_by_id(model_id: int, db: Annotated[Session, Depends(get_db)]):
    return crud_models.get_model_by_id(model_id, db)

@router.post("/new", response_model=ModelCreate)
def create_model(model: ModelCreate,db: Annotated[Session, Depends(get_db)]):
    model = models.Model(name=model.name)
    db.add(model)
    db.commit()
    return model

@router.get("/", response_model=List[schemas.Model], dependencies=[Depends(get_current_admin_user)])
def list_models(db: Session = Depends(get_db)):
    return crud_models.get_models(db)

