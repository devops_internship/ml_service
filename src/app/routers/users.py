from typing import Annotated, List
from fastapi import APIRouter, Body, Depends
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from app.schemas import schemas
from app.utils.database import get_db
from ..crud import users as crud_users

router = APIRouter(
    prefix="/users",
    tags=['users'],
    responses={404: {"description": "Not found."}}
)

@router.get("/me", response_model=schemas.UserBase)
def read_current_user_info(user: Annotated[schemas.UserBase, Depends(crud_users.get_current_user)]):
    return user

@router.get("/{user_id}", dependencies=[Depends(crud_users.get_current_admin_user)])
def read_user_info(user_id: int, db: Annotated[Session, Depends(get_db)]):
    return crud_users.get_user_by_id(user_id, db)

@router.post("/token")
def login_for_access_token(form_data: Annotated[OAuth2PasswordRequestForm, Depends()], db: Annotated[Session, Depends(get_db)]):
    return crud_users.login(form_data, db)

@router.post("/register", response_model=schemas.UserBase)
def register(user: Annotated[schemas.UserCreate, Body()], db: Session = Depends(get_db)):
    return crud_users.create_user(user, db)

@router.get("/",response_model=List[schemas.UserBase], dependencies=[Depends(crud_users.get_current_admin_user)])
def read_all_users(db: Session = Depends(get_db)):
    return crud_users.get_all_users(db)
