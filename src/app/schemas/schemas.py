from typing import List, Optional
from pydantic import BaseModel

class UserBase(BaseModel):
    username: str

class User(UserBase):
    id: int
    hashed_password: str
    admin: bool

    class Config:
        orm_mode = True

class UserCreate(UserBase):
    password: str
    admin: bool

class MetricBase(BaseModel):
    name: str
    metric_type: str
    value: str

class MetricCreate(MetricBase):
    pass

class Metric(MetricBase):
    class Config:
        orm_mode = True

class ExperimentBase(BaseModel):
    description: str
    model_id: int
    user_id: int

class ExperimentCreate(ExperimentBase):
    metrics: Optional[List[MetricCreate]] = []

class Experiment(ExperimentBase):
    id: int
    metrics: List[Metric] = []

    class Config:
        orm_mode = True

class ModelBase(BaseModel):
    name: str

class ModelCreate(ModelBase):
    pass

class Model(ModelBase):
    id: int

    class Config:
        orm_mode = True   
