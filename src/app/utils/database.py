from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
import os

from sqlalchemy.orm import sessionmaker


DATABASE_URL = os.getenv("DATABASE_URL", default="postgresql+psycopg2://kuba:kuba@localhost:5432/database")
engine = create_engine(DATABASE_URL)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

